<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>登录</title>
    <meta name="description" content="particles.js is a lightweight JavaScript library for creating particles.">
    <meta name="author" content="Vincent Garreau"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" media="screen" href="resources/css/style.css">
    <link rel="stylesheet" type="text/css" href="resources/css/reset.css"/>
</head>
<body>

<div id="particles-js">
    <div class="login">
        <div class="login-top">
            用户登录
        </div>
        <form class="form-horizontal m-t-20" method="POST" id="form">
            <div class="login-center clearfix">
                <div class="login-center-img"><img src="resources/img/name.png"/></div>
                <div class="form-group">
                    <div class="login-center-input">
                        <input type="text" name="loginid" value="" placeholder="请输入您的用户名" onfocus="this.placeholder=''" id="loginid"
                               onblur="this.placeholder='请输入您的用户名'"/>
                        <div class="login-center-input-text">用户名</div>
                    </div>
                </div>
            </div>
            <div class="login-center clearfix">
                <div class="login-center-img"><img src="resources/img/password.png"/></div>
                <div class="form-group">
                    <div class="login-center-input">
                        <input type="password" name="password" value="" placeholder="请输入您的密码" onfocus="this.placeholder=''" id="password"
                               onblur="this.placeholder='请输入您的密码'"/>
                        <div class="login-center-input-text">密码</div>
                    </div>
                </div>
            </div>
            <div class="login-button">
                <a href="login">登陆</a>
            </div>

            <div class="register-button">
                <a href="/register">注册</a>
            </div>
        </form>
    </div>
    <div class="sk-rotating-plane"></div>
</div>

<!-- scripts -->
<script src="resources/js/jquery-1.11.1.min.js"></script>
<script src="resources/js/particles.min.js"></script>
<script src="resources/js/app.js"></script>

<%--<script type="text/javascript">--%>
    <%--function hasClass(elem, cls) {--%>
        <%--cls = cls || '';--%>
        <%--if (cls.replace(/\s/g, '').length == 0) return false; //当cls没有参数时，返回false--%>
        <%--return new RegExp(' ' + cls + ' ').test(' ' + elem.className + ' ');--%>
    <%--}--%>

    <%--function addClass(ele, cls) {--%>
        <%--if (!hasClass(ele, cls)) {--%>
            <%--ele.className = ele.className == '' ? cls : ele.className + ' ' + cls;--%>
        <%--}--%>
    <%--}--%>

    <%--function removeClass(ele, cls) {--%>
        <%--if (hasClass(ele, cls)) {--%>
            <%--var newClass = ' ' + ele.className.replace(/[\t\r\n]/g, '') + ' ';--%>
            <%--while (newClass.indexOf(' ' + cls + ' ') >= 0) {--%>
                <%--newClass = newClass.replace(' ' + cls + ' ', ' ');--%>
            <%--}--%>
            <%--ele.className = newClass.replace(/^\s+|\s+$/g, '');--%>
        <%--}--%>
    <%--}--%>

    <%--document.querySelector(".login-button").onclick = function () {--%>
        <%--addClass(document.querySelector(".login"), "active")--%>
        <%--setTimeout(function () {--%>
            <%--addClass(document.querySelector(".sk-rotating-plane"), "active")--%>
            <%--document.querySelector(".login").style.display = "none"--%>
        <%--}, 800);--%>

        <%--var formdata =  $("#form").serializeArray();--%>
        <%--console.log(formdata)--%>

        <%--$.ajax({--%>
            <%--type : "POST",--%>
            <%--data : formdata,--%>
            <%--url : 	"/login",--%>
            <%--dataType : 'json',--%>
            <%--async : true,--%>
            <%--success : function(data) {--%>
                <%--console.log(data)--%>
                <%--debugger--%>
                <%--if(data.status == 0){--%>
                    <%--setTimeout(function () {--%>
                        <%--// removeClass(document.querySelector(".login"), "active")--%>
                        <%--// removeClass(document.querySelector(".sk-rotating-plane"), "active")--%>
                        <%--// document.querySelector(".login").style.display = "block"--%>
                        <%--alert("登录成功")--%>

                    <%--}, 5000)--%>

                    <%--location.href = "/index"--%>
                <%--}--%>

            <%--},--%>
            <%--error : function (date) {--%>
                <%--console.log(data);--%>
            <%--}--%>
        <%--});--%>

    <%--}--%>

    <%--document.querySelector(".register-button").onclick = function () {--%>

    <%--}--%>

<%--</script>--%>
<div style="text-align:center;">
</div>
</body>
</html>