package com.spring.aop.main;

import com.spring.aop.service.impl.LoginServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.aop.framework.AopProxy;

/**
 * @author zhen.sun@hand-china.com
 * @version 1.0
 * @ClassName AOPTest
 * @date Created on 2019/3/2 18:16
 */
public class AOPTest {
    public static void main(String[] args) {
//        ProxyFactory proxyFactory = new ProxyFactory();     // 创建代理工厂
//        proxyFactory.setTarget(new GreetingImpl());         // 射入目标类对象
//        proxyFactory.addAdvice(new GreetingBeforeAdvice()); // 添加前置增强
//        proxyFactory.addAdvice(new GreetingAfterAdvice());  // 添加后置增强
//        proxyFactory.addAdvice(new GreetingAroundAdvice()); // 添加环绕增强
//        proxyFactory.addAdvice(new GreetingThrowAdvice()); // 添加异常增强
//
//        Greeting greeting = (Greeting) proxyFactory.getProxy(); // 从代理工厂中获取代理
//        greeting.sayHello("Jack");                              // 调用代理的方法

//        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml"); // 获取 Spring Context
//        GreetingImpl greeting = (GreetingImpl)context.getBean("greetingProxy");      // 从 Context 中根据 id 获取 Bean 对象（其实就是一个代理）
//        greeting.sayHello("Jack");                                         // 调用代理的方法
//        greeting.goodMorning("aa");

                ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml"); // 获取 Spring Context
        LoginServiceImpl greeting = (LoginServiceImpl)context.getBean("loginServiceImpl");      // 从 Context 中根据 id 获取 Bean
        greeting.login();
    }
}
