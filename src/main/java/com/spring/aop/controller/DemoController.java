package com.spring.aop.controller;

import com.spring.aop.service.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author zhen.sun@hand-china.com
 * @version 1.0
 * @ClassName DemoController
 * @date Created on 2019/3/2 16:03
 */

@Controller
public class DemoController {

    @Autowired
    private ILoginService ILoginService;

    @RequestMapping("/main")
    public String index() {
        return "main";
    }

    @RequestMapping("/login")
    public String login() {
        ILoginService.login();
        return "main";
    }
}
