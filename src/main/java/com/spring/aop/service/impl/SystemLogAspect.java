package com.spring.aop.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.spring.aop.annotation.SystemLog;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;


/**
 * @author zhen.sun@hand-china.com
 * @version 1.0
 * @ClassName SystemLogAspect
 * @date Created on 2019/3/2 22:38
 */
@Aspect
@Component
public class SystemLogAspect {

    //本地异常日志记录对象
    private final Logger logger = Logger.getLogger(this.getClass());

    /**
     * 环绕通知
     *
     * @param pjp 切入点
     */
    @Around("@annotation(com.spring.aop.annotation.SystemLog)")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        StringBuilder log = new StringBuilder("around: ");
        //类名
        String className = pjp.getTarget().getClass().getName();
        //方法名
        String methodName = pjp.getSignature().getName();

        Object result = pjp.proceed();

        log.append("className: ")
                .append(className)
                .append(",  methodName: ")
                .append(methodName)
                .append(",  sparams: ");
        //参数
        Object[] args = pjp.getArgs();
        for (Object arg : args) {
            log.append(JSONObject.toJSONString(arg) + ", ");
        }
        logger.info(log.toString());
        return result;
    }

    /**
     * 异常通知 用于拦截层记录异常日志
     *
     * @param e 异常
     */
    @AfterThrowing(value = "@annotation(com.spring.aop.annotation.SystemLog)", throwing = "e")
    public void afterThrowing(Throwable e) {
        logger.error("afterThrowing: " + e.getMessage(), e);
    }

    /**
     * 获取注解中对方法的描述信息
     *
     * @param joinPoint 切点
     * @return 方法描述
     * @throws Exception
     */
    public static String getServiceMethodDescription(JoinPoint joinPoint)
            throws Exception {
        String targetName = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        Object[] arguments = joinPoint.getArgs();
        Class targetClass = Class.forName(targetName);
        Method[] methods = targetClass.getMethods();
        String description = "";
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                Class[] clazzs = method.getParameterTypes();
                if (clazzs.length == arguments.length) {
                    description = method.getAnnotation(SystemLog.class).description();
                    break;
                }
            }
        }
        return description;
    }
}
