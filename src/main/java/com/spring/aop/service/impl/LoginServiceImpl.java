package com.spring.aop.service.impl;

import com.spring.aop.annotation.SystemLog;
import com.spring.aop.service.ILoginService;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @author zhen.sun@hand-china.com
 * @version 1.0
 * @ClassName LoginServiceImpl
 * @date Created on 2019/3/2 21:58
 */
@Service
@Component
public class LoginServiceImpl implements ILoginService{

    @SystemLog(description="日志测试")
    @Override
    public void login(){
        System.out.println(" 登陆成功！");
        //throw new RuntimeException("Error");
    }
}
