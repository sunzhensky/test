package com.spring.aop.service;

/**
 * @author zhen.sun@hand-china.com
 * @version 1.0
 * @ClassName ILoginService
 * @date Created on 2019/3/2 21:57
 */
public interface ILoginService {
    void login();
}
